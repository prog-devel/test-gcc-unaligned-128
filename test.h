#pragma once

#include <cstdint>

//#define FIX 4

#if FIX==1
#pragma pack(push, 1)
#endif
struct Base128 {
	__int128_t v
#if FIX==3
	;
#else
	{0};
#endif
};
#if FIX==1
#pragma pack(pop)
#endif

#if FIX==2
#else
#pragma pack(push, 1)
#endif
struct Int128: Base128 {
	Int128();
} 
#if FIX==2
__attribute__((packed, aligned(1)));
#else
;
#pragma pack(pop)
#endif

struct A {
	uint8_t m8;	
#if FIX==5
	Int128 __attribute__((aligned(16))) m128;
#else
	Int128 m128;	
#endif
};



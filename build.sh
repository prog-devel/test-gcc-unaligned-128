#!/usr/bin/env bash

ulimit -c unlimited

if [ -n "$1" ]; then
	FIX="$1"
else
	FIX=0
fi

#g++-7 -std=c++14 -fsanitize=address -O2 -fno-omit-frame-pointer -g test.cpp main.cpp -DFIX="$FIX"  && ./a.out
g++-7 --save-temps -Wall -Wextra -std=c++14 -O2 -g main.cpp test.cpp -DFIX="$FIX" && ./a.out

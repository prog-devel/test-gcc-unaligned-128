#include <new>

#ifdef DUMP
#include <iostream>
#endif

#include "test.h"

#if FIX==4
Int128::Int128() { } // inline optimization?
#else
#endif

int main() {
#ifdef DUMP
	std::cerr << "main.cpp" << std::endl;
	std::cerr << alignof(A) << " " << sizeof(A) << std::endl;

	std::cerr << offsetof(A, m8) << std::endl;
	std::cerr << offsetof(A, m128) << std::endl;
	std::cerr << offsetof(A, m128.v) << std::endl;
#endif

	A a;

	//auto* pa = new A;

	//unsigned char paa[sizeof(A)];
	//new (paa) A;
	
	return 0;
}
